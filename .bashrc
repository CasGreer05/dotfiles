#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


export HISTSIZE=5000
export HISTCONTROL=ignoredups:erasedups           # no duplicate entries

set -o vi

### ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

alias ls='ls --color=auto'
alias vim='nvim'
alias sudo='doas'
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'

# PS1='[\u@\h \W]\$ '
 PS1='\[\e[0;37m\][\[\e[0;2;95m\]\u\[\e[0;2;37m\]@\[\e[0;2;96m\]\H \[\e[0;37m\]\t \[\e[0;1;93m\]\w\[\e[0;37m\]]\[\e[0;1;37m\]\$ \[\e[0m\]'


export PATH="$HOME/.emacs.d/bin:$PATH"

